-- > To fully enable all features of modern JavaScript, we should start scripts with "use strict".
'use strict';

console.log(typeof 4.5)
    // → number
console.log(typeof "x")
    // → string

// careful, typeof returns "object" for null... because why not

-- > Interaction
We’ re using a browser as a working environment, so basic UI functions will be:

    prompt(question[,
        default])
Ask a question, and
return either what the visitor entered or null
if he pressed“ cancel”.

confirm(question)
Ask a question and suggest to choose between Ok and Cancel.The choice is returned as true / false.

alert(message)
Output a message.

All these functions are modal, they pause the code execution and prevent the visitor from interacting with the page until he answers.


-- > Random stuff

strings can be made with: "", '', ``

with ``
you can put variable expressions inside $ {}
`half of 100 is ${100 / 2}`

console.log("Itchy" != "Scratchy")
    // → true
console.log("Apple" == "Orange")
    // → false

NaN - > Not a Number(result of 1 / 0
    for example): the result of a nonsensical computation

ternary operator:
    chooseOne = true
console.log(chooseOne ? 1 : 0)
    // 1

two empty values that should be treated the same: there are two because why not
null, undefined

-- - > Kinda confusing automatic type conversion

console.log(8 * null)
    // → 0
console.log("5" - 1)
    // → 4
console.log("5" + 1)
    // → 51
console.log("five" * 2)
    // → NaN
console.log(false == 0)
    // → true

When you do not want any type conversions to happen, there are two additional operators:
    ===
    and !==
    The first tests whether a value is precisely equal to the other,
    and the second tests whether it is not precisely equal.So "" === false is false as expected.

If you have a value that might be empty, you can put || after it with a replacement value.
If the initial value can be converted to false, you’ ll get the replacement instead.

The rules
for converting strings and numbers to Boolean values state that 0, NaN, and the empty string("") count as false,
    while all the other values count as true.So 0 || -1 produces - 1, and "" || "!?"
yields "!?".

Summary:
    conversion to number:
    undefined - > NaN
null - > 0
true / false - > 1 / 0
string - > removes whitespaces and reads the string;
empty string is 0;
any error gives NaN
conversion to boolean:
    0, null, undefined, NaN, "" - > false
any other value - > true

-- -- > Comparisons

Strings are compared letter by letter, unicode order
    'Z' > 'A' // true
Different types, they are converted to numbers
    '2' > 1 // true, '2' becomes 2
false == 0 // true, false becomes 0

    ===
    or !== to avoid type conversions!
    false === 0 // false

strange cases:
    null == undefined // true
    // comparing null or undefined, only yields true if both are null or undefined

Evading problems: careful when you compare variables that might be undefined / null

-- - > Variables

Old variable declarations:
    var - > they have no block scope

New variable declarations: (with block scope)(2015, colorized)
let,
const

    example: {
        let x = 2;
    }
    // x can NOT be used here

Just use
let instead of
var

let one = 1,
    two = 2;
console.log(one + two);
// → 3

The word
const stands
for constant.It defines a constant binding, which points at the same value
for as long as it lives.
This is useful
for bindings that give a name to a value so that you can easily refer to it later.

-- -- > Some functions

prompt("Enter passcode");

converter functions:
    Number(), String(), etc

Number.isNaN()

-- - > FLOW CONTROL

for (let number = 0; number <= 12; number = number + 2) {
    console.log(number);
}

for (let entry of array) {
    console.log(entry);
}

switch (prompt("What is the weather like?")) {
    case "rainy":
        console.log("Remember to bring an umbrella.");
        break;
    case "cloudy":
        console.log("Go outside.");
        break;
    default:
        console.log("Unknown weather type!");
        break;
}

"string".length

while (true) {}

do {} while (true);

-- > FUNCTIONS

const square = function(x) {
    return x * x;
};

//shorter syntax:
function future() {
    return "You'll never have flying cars";
}

That type of definition won 't follow the top down flow,
the funciton will be available before its declaration

//arrow syntax:
const square1 = (x) => { return x * x; };
const square2 = x => x * x;

//those functions can be binded to a variable with "let" so you can change the function:
let method = function() { return "hi" }

method = function() { return "changed" }

// if too many arguments are passed the extra ones are ignored
// if too few are passed, the missing ones are undefined

function minus(a, b) {
    if (b === undefined) return -a;
    else return a - b;
}

// default arguments:
function power(base, exponent = 2)

// closures: being able to reference variables created in another context
// in this example: returns a function which multiplies by a variable created in the enclosing multiplier fun.

function multiplier(factor) {
    return number => number * factor;
}

let twice = multiplier(2);
console.log(twice(5));
// → 10


-- -- -- - > Arrays

let sequence = [1, 2, 3];
let sequenceOfObjects = [{ x: 4, y: 5 }]

// these are methods
sequence.includes(2) // true

array.push(5) // add to the end of the array
array.pop() // returns and removes the last value

array.unshift(something) // adds at the start
array.shift() // removes at the start

array.indexOf(2) // returns -1 if 2 is not found
array.lastIndexOf(2) // starts search from the end
    // both have optional 2nd argument to where it starts searching from

array.slice(2, 4) // returns sub-array from 2nd (inclusive) to 4 (exclusive)
console.log([0, 1, 2, 3, 4].slice(2, 4));
// → [2, 3]
console.log([0, 1, 2, 3, 4].slice(2));
// → [2, 3, 4]

array.slice() //copies the entire array

array3 = array.concat(array2) //adds arrays

array.reverse()

-- -- - > Strings

string.toUpperCase()
string.length
string.slice(3, 4) //like array
string.indexOf("let") //like array, but can search for more than one character

string.trim() // removes whitespace (spaces, newlines, tabs, and similar characters) from the start and end of a string

String(6).padStart(3, "0"));
// → 006

// SPLIT AND JOIN
let sentence = "Secretarybirds specialize in stomping";
let words = sentence.split(" ");
console.log(words);
// → ["Secretarybirds", "specialize", "in", "stomping"]
console.log(words.join(". "));
// → Secretarybirds. specialize. in. stomping

"la".repeat(34)
    // lalalalalalala....

string[3] // 4th character


-- - > Object

let day = {
    // properties of object, key: value
    squirrel: false,
    events: ["work", "touched tree", "pizza", "running"]
};

let descriptions = {
    work: "Went to work",
    "touched tree": "Touched a tree" [varName]: 5, // computed property, the name is whatever varName is at the moment
    name, // property with same name and same value as the variable name, instead of writing "name: name,"

    sizes: { // can have nested objects too
        height: 182,
        width: 50
    }
};

-- -- > Properties(variables inside an object)

value.x // get prop. named "x"
value[x] // get prop. named whatever x is; evaluates expression x

// properties are strings, with dot notation you can't access names like 2, John Doe
// so for those properties you need the square bracket notation
value[2], value["John Doe"]

// therefore you could use array["length"] but it is easier to write array.length

// remove an object property
delete descriptions.work
    // get all properties of an object:
Object.keys(descriptions)

// looping through all properties of an object
for (key in object) {
    // executes the body for each key among object properties
}

// check if an object has a property:
"work" in descriptions

// accessing a unexisting property will return "undefined"

// copy all props. from one object into another:
Object.assign(objectA, objectB)

// comparing objects:
object1 == object2 - > will check
if they refer to the same object
// there isn't built-in "equals()" method like in java, to compare equality based on contents

// changing properties later:
let obj = {};
obj.value = 4;
obj.name = "whatever";

// copies all properties from permissions1 and permissions2 into user. if property of same name exists in user, it is overwritten
Object.assign(user, permissions1, permissions2);

// quickly cloning an object
let clone = Object.assign({}, user);

// note that cloning will not actually clone properties with references to objects
//(the reference will be copied so the clone will reference to same object, but will not actually clone that object to a new one)
// for that deep cloning is needed: we should use the cloning loop that examines each value of user[key] and,
// if it’s an object, then replicate its structure as well


-- -- > Object methods

let user = {
    name: "John",
    age: 30
};

user.sayHi = function() {
    alert("Hello!");
};

user.sayHi(); // Hello!

// declaring it inside the object
let user = {
    name: "Manolo",

    sayHi() { // same as "sayHi: function()"
        alert(this.name); // use "this" to access object's variables
        // this is evaluated on runtime. It can be anything. returns undefined if not inside an object
    }
};

-- -- > PASS BY VALUE
// ignore lack of ;
let x = 5;
let data = { name = "something" }
doStuff(x, data)

function doStuff(number, object) {
    number = 1; // won't change the x variable, it passed its value (number 5), you can't affect the value of x itself
    object = {} // won't affect object "data", it passed its value (reference to the object {name = "something"})
        // you can't change what the variable "data" itself references to, you only have the value of what its reference is

    object.name = "changed!" // though you can change things inside that object, since you have the reference to it
}

data.name // it's "changed!"

-- - > Spread operator

function max(...numbers) - > numbers is an array with all the args passed
let numbers = [5, 1, 7];

console.log(max(...numbers)); - > spreads numbers array contents to arguments
// → 7

let words = ["never", "fully"];
console.log(["will", ...words, "understand"]);
// → ["will", "never", "fully", "understand"]

-- - > Math

Math.cos, sin, tan, acos, asin, atan, PI, random() - > value between 0 and 1(not included), floor, ceil, round,

    -- -- > Json

Json.stringify(object)
Json.parse(string)


-- -- - > Automated testing


describe("pow", function() {

            it("raises to n-th power", function() {
                assert.equal(pow(2, 3), 8);
            });

            // you can nest tests
            describe("child test", function() {
                    it("whatever", function() {
                        assert.equal(pow(2, 3), 8);
                    });

                    // force to run only this test: it.only
                    it.only("5 in the power of 2 equals 25", function() {
                        assert.equal(pow(5, 2), 25);
                    });
                }
            });