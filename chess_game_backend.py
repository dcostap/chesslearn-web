import json
import string
import random
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from flask import Flask, jsonify, redirect, request
from flask_socketio import SocketIO
from flask_socketio import send, emit
from enum import Enum
from itertools import repeat
import chess

games = []
rooms_of_players = {}  # associates a player id with its websocket room id


def init(socketio):
    @socketio.on('connect')
    def connect():
        pass

    @socketio.on('disconnect')
    def disconnect():
        print(request.sid + " disconnected!")

        player_id = None
        for key, value in rooms_of_players.items():
            if value == request.sid:
                player_id = key
                break

        if player_id is not None:
            game_affected = next(
                (g for g in games if player_id in (g.player1.id, g.player2.id)), None)
            print("affected game: " + str(game_affected))

            if game_affected is not None:
                game_affected.one_player_left()

                games.remove(game_affected)
        pass

    @socketio.on('new_game')
    def handle_new_game(event):
        id = random_string()
        rooms_of_players[id] = request.sid
        new_game = Game(id)
        games.append(new_game)
        emit("joined_game", {"player_id": id, "game_id": new_game.game_id})

    @socketio.on('join_game')
    def handle_join_game(event):
        game_id = event["game_id"]
        the_game = next((g for g in games if g.game_id == game_id), None)
        id = random_string()
        rooms_of_players[id] = request.sid

        if the_game is not None and the_game.player2 is None:
            emit("joined_game", {
                "player_id": id,
                 "game_id": the_game.game_id,
                 "game_full": True
                 })
            the_game.player2 = Player(id)
            the_game.start()
        else:
            emit("error_joining_game", {

            })

    @socketio.on('move')
    def handle_move(event):
        game_id = event["game_id"]
        player_id = event["player_id"]
        move_san = event["move_san"]

        the_game = next((g for g in games if g.game_id == game_id), None)

        if the_game is not None and player_id in [the_game.player1.id, the_game.player2.id]:
            the_game.move(move_san, player_id)

    @socketio.on('send_message')
    def handle_send_message(event):
        game_id = event["game_id"]
        player_id = event["player_id"]
        text = event["text"]

        text = text.strip()

        the_game = next((g for g in games if g.game_id == game_id), None)

        if the_game is not None and player_id in [the_game.player1.id, the_game.player2.id]:
            emit("new_message", {
                "is_yours": True,
                "text": text
                }, room=rooms_of_players[player_id])

            emit("new_message", {
                "is_yours": False,
                "text": text
                }, room=rooms_of_players[the_game.player2.id if player_id == the_game.player1.id else the_game.player1.id])


def random_string():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))


class Player:
    def __init__(self, id):
        self.id = id


class Game:
    def __init__(self, player1):
        self.game_id = random_string()
        self.player1 = Player(player1)
        self.player2 = None
        self.isPlayer1White = random.choice([True, False])

        self.cards = []

    def start(self):
        self.board = chess.Board()

        self.emit_new_turn()

    def emit_new_turn(self):
        print("New turn; to move: " + str(self.board.turn))
        isPlayer1Turn = (self.isPlayer1White and self.board.turn == chess.WHITE) or (
            not self.isPlayer1White and self.board.turn == chess.BLACK)
        print("isplayer1turn: " + str(isPlayer1Turn))
        emit("new_turn", {
            "fen": self.board.fen(),
            "is_your_turn": isPlayer1Turn
        }, room=rooms_of_players[self.player1.id])
        emit("new_turn", {
            "fen": self.board.fen(),
            "is_your_turn": not isPlayer1Turn
        }, room=rooms_of_players[self.player2.id])

    def one_player_left(self):
        for p in (self.player1.id, self.player2.id):
            if p is not None:
                emit("disconnect", {}, room=rooms_of_players[p])

    def move(self, move_san, player_id):
        the_player = self.player1 if player_id == self.player1.id else self.player2
        is_the_player_white = (self.isPlayer1White and the_player == self.player1) or (
            not self.isPlayer1White and the_player == self.player2)

        print("Processing a move: " + str(move_san))
        print("is_the_player_white: " + str(is_the_player_white))

        def emit_invalid_move():
            emit("new_turn", {
                "fen": self.board.fen(),
                "is_your_turn": True,
                "last_move_was_invalid": True,
            }, room=rooms_of_players[player_id])

        if (self.board.turn == chess.WHITE) != is_the_player_white:
            emit_invalid_move()
            return

        previous_fen = self.board.fen()
        try:
            move_done = self.board.push_san(move_san)

            if not self.board.is_valid():
                emit_invalid_move()
                self.board.set_fen(previous_fen)
            else:
                print("Move done on board: " + str(move_done))

                if self.board.outcome() is None:
                    self.emit_new_turn()
                else:
                    self.onGameEnded()
        except Exception as e:
            print(str(e))
            emit_invalid_move()
            pass

    def onGameEnded(self):
        info = {
            "fen": self.board.fen(),
            "winner_color": self.board.outcome().winner
        }
        emit("game_end", info, room=rooms_of_players[self.player1.id])
        emit("game_end", info, room=rooms_of_players[self.player2.id])

        rooms_of_players.pop(self.player1.id, None)
        rooms_of_players.pop(self.player2.id, None)
        games.remove(self)
        print("Finished a game; players remaining in backend: " +
              str(rooms_of_players))
        print("games remaining: " + str(games))
