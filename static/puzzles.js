let socket = undefined
let reconnectTimer = undefined

let currentPuzzle = undefined
let currentPuzzleMoveCounter = 0

let chessEngine = undefined
let board = undefined
let is_my_turn = undefined
let am_I_white_player = undefined

document.addEventListener("DOMContentLoaded", function (event) {
    // starting position
    processNewFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
    fetchNewPuzzleInfo()
})

async function fetchNewPuzzleInfo() {
    $("#puzzle-info").empty()

    $("#puzzle-info").append(
        `
        <div class="animate__animated animate__bounceIn">
        <div class="block is-size-4 has-text-centered">Loading new puzzle...</div>
        <div class="block">
            <progress class="progress is-small is-primary" max="100"></progress>
        </div>
        </div>
        `
    )

    fetch('new_puzzle')
        .then(response => {
            console.log(response);
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(blob => {
            console.log("received puzzle: " + blob)
            currentPuzzle = blob
            setupCurrentPuzzleInfo()
        })
        .catch(e => {
            console.log('There has been a problem with your fetch operation: ' + e.message);

            $("#puzzle-info").empty()

            $("#puzzle-info").append(
                `
                <div class="animate__animated animate__bounceIn">
                <div class="block is-size-4 has-text-centered">There was a problem trying to establish connection with the server...</div>
                </div>
                `
            )

            setTimeout(() => {
                fetchNewPuzzleInfo()
            }, 10000);
        });
}

function setupCurrentPuzzleInfo() {
    $("#puzzle-info").empty()
    is_my_turn = true;
    processNewFen(currentPuzzle["fen"])

    let color = am_I_white_player ? "white" : "black";

    $("#puzzle-info").append(`
    <div class="animate__animated animate__bounceIn">
        <div class="block has-text-centered m-4">
            <span class="icon">
                <i class="fas fa-2x fa-search"></i>
            </span>
            <span class="has-text-centered ml-4">
                <span class="is-size-5">Find the right move for ${color}!</span>
            </span>
        </div>

        <div class="block has-text-centered" id="move_info_div" style="height: 100%" />

        <div class="block has-text-centered" style="height: 100%">
            <button class="button is-primary is-outlined" onclick="onNewPuzzleClick()">
            New puzzle
            <i class="fas fa-arrow-right ml-3"></i>
            </button>
        </div>
        </div>
    `)
}

function onNewPuzzleClick() {
    fetchNewPuzzleInfo()
}

function processNewFen(fen) {
    if (chessEngine == undefined || board == undefined) {
        initDisplayedBoard(fen)
    } else {
        updateDisplayedBoardToFEN(fen)
    }
}

function updateDisplayedBoardToFEN(fen) {
    chessEngine = new Chess(fen)
    console.log("Validity of the fen position: " + chessEngine.validate_fen(fen))

    updateDisplayedBoardToEngine()
}

function updateDisplayedBoardToEngine() {
    board.position(chessEngine.fen())

    if (is_my_turn)
        am_I_white_player = chessEngine.turn() != "b"
    else
        am_I_white_player = chessEngine.turn() == "b"

    my_name = currentPuzzle == undefined ? "" : "<span class='has-text-info'>You</span>"
    anon = ""

    $("#top_of_board_name").html(am_I_white_player ? anon : my_name)
    $("#bottom_of_board_name").html(!am_I_white_player ? anon : my_name)

    // repetitive code ahead, I'm too lazy to clean it
    $("#top_of_board_icon").removeClass("fa-robot")
    $("#top_of_board_icon").removeClass("fa-user-alt")
    $("#bottom_of_board_icon").removeClass("fa-robot")
    $("#bottom_of_board_icon").removeClass("fa-user-alt")

    $("#top_of_board_icon").addClass(am_I_white_player ? "fa-robot" : "fa-user-alt")
    $("#bottom_of_board_icon").addClass(!am_I_white_player ? "fa-robot" : "fa-user-alt")

    $("#top_of_board_after_name").empty()
    $("#bottom_of_board_after_name").empty()
    let element = chessEngine.turn() == "b" ? $("#top_of_board_after_name") : $("#bottom_of_board_after_name")

    if (currentPuzzle != undefined) {
        let text = is_my_turn ? "Your turn" : "His turn"
        let color = is_my_turn ? "is-info" : "is-danger"

        element.html(`
        <span class="tag ${color} is-medium ml-3">${text}</span>
    `)
    }
}

function onMadeMove(san) {
    if (currentPuzzle != undefined) {
        let cont = currentPuzzle["continuation"]
        console.log(currentPuzzle);

        console.log("Comparing move made: " + san + "; with move from puzzle data: " + cont[currentPuzzleMoveCounter]);
        let valid = cont[currentPuzzleMoveCounter] == san;

        setTimeout(function () {
            if (!valid) {
                chessEngine.undo()
                updateDisplayedBoardToEngine()

                $("#move_info_div").empty()
                $("#move_info_div").append(`
                    <span class="icon-text animate__animated animate__headShake">
                        <span class="icon has-text-danger">
                            <i class="fas fa-2x fa-times"></i>
                        </span>
                        <span class="is-size-4 ml-3">Wrong move!</span>
                    </span>
                `)
            } else {
                currentPuzzleMoveCounter++;
                if (cont.length > currentPuzzleMoveCounter) {
                    chessEngine.move(cont[currentPuzzleMoveCounter])
                    currentPuzzleMoveCounter++;
                    $("#move_info_div").empty()
                    $("#move_info_div").append(`
                        <span class="icon-text">
                            <span class="icon has-text-success">
                                <i class="fas fa-2x fa-check"></i>
                            </span>
                            <span class="is-size-4 ml-3">Correct move!</span>
                        </span>
                    `)
                } else {
                    console.log("Finished puzzle");

                    $("#move_info_div").empty()
                    $("#move_info_div").append(`
                        <span class="icon-text">
                            <span class="icon has-text-success">
                                <i class="fas fa-2x fa-check"></i>
                            </span>
                            <span class="is-size-4 ml-3">Puzzle finished!</span>
                        </span>
                    `)
                }
                updateDisplayedBoardToEngine()
            }
        }, 180)
    }

    clearAllNotifications()
}

function clearAllNotifications() {
    $(".invalid-id-not").each(function (index, element) {
        element.remove()
    })
}

function initDisplayedBoard(fen) {
    $.get("html_snippets/chessboard_div.html", function (data) {
        $("#contents").empty();
        $("#contents").css("display", "block");
        $("#contents").append(data)

        var config = {
            draggable: true,
            position: 'start',
            onDragStart: onDragStart,
            onDrop: onDrop,
            onSnapEnd: onSnapEnd
        }

        function onDragStart(source, piece, position, orientation) {
            // do not pick up pieces if the game is over
            if (chessEngine.game_over()) return false

            // only pick up pieces for the side to move
            if (am_I_white_player == undefined) return true;

            return (
                ((chessEngine.turn() === 'w' && am_I_white_player) || (chessEngine.turn() === 'b' && !am_I_white_player))
                &&
                (piece[0] === chessEngine.turn()) // can only drag my pieces
            )
        }

        function onDrop(source, target) {
            // see if the move is legal
            var move = chessEngine.move({
                from: source,
                to: target,
                promotion: 'q' // NOTE: always promote to a queen for example simplicity
            })

            // illegal move
            if (move === null) {
                console.log("Made an invalid move");
                return 'snapback'
            }

            console.log("Made a valid move: " + move);
            onMadeMove(move.san)
        }

        // update the board position after the piece snap
        // for castling, en passant, pawn promotion
        function onSnapEnd() {
            board.position(chessEngine.fen())
        }

        board = Chessboard('board', config);
        updateBoardSizing()
        updateDisplayedBoardToFEN(fen)
    })
}

function updateBoardSizing() {
    // don't let the board get out of the view
    $("#chess-div").css({
        "width": "60vmin",
    })
    if (board) {
        board.resize()
    }
}

window.onresize = function () {
    updateBoardSizing()
}