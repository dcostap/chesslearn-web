$.get("html_snippets/navbar.html", function(data) {
    $("#header").replaceWith(data);
    $("body").css("visibility", "visible")

    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function () {
        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

    updateThemeSwitcher()
})

// themes button with dropdown list
function onThemesButtonClick() {
    $('#themes-dropdown').toggleClass('is-active')
}

// close dropdown list if clicked anywhere outside it
window.addEventListener('click', function (e) {
    if (document.getElementById('themes-dropdown').contains(e.target)) {

    } else {
        $('#themes-dropdown').toggleClass('is-active', false)
    }
});