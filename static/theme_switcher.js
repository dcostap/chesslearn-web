let styles = ["css/light.css", "css/dark.css"];
let boardThemes = [1, 2]

function updateThemeSwitcher() {
    let currentTheme = getCurrentTheme()
    for (theme in boardThemes) {
        let themeNumber = boardThemes[theme]
        let button = $("#theme_button_" + themeNumber)

        if (currentTheme == themeNumber) {
            button.toggleClass("is-success", true)
            button.toggleClass("is-outlined", false)
        } else {
            button.toggleClass("is-outlined", true)
            button.toggleClass("is-success", false)
        }

        button.off('click') // remove listener if it already exists
        button.on("click", function (event) {
            onChooseBoardTheme(themeNumber)
        })
    }
}

function onDarkThemeButtonClick() {
    let currentCssStyle = document.getElementById('style');

    let previousStyle = currentCssStyle.href;
    if (previousStyle.endsWith(styles[0]))
        newStyle = styles[1];
    else
        newStyle = styles[0];

    localStorage.setItem("style", newStyle);
    $('body').fadeOut(60);

    setTimeout(function() {
        updateThemes()
        $('body').fadeIn(60);
    }, 70)
}

function onChooseBoardTheme(themeNumber) {
    localStorage.setItem("theme", themeNumber)
    updateThemes()
}

function getCurrentTheme() {
    let theme = localStorage.getItem("theme")
    theme = parseInt(theme)
    return theme
}

function updateThemes() {
    let theme = getCurrentTheme()

    if (!boardThemes.includes(theme)) theme = boardThemes[0]
    let tag = document.getElementById('board_theme');
    tag.href = "chessboard/css/board-theme" + theme + ".css";

    let style = localStorage.getItem("style")

    if (!styles.includes(style)) style = styles[0]
    let currentCssStyle = document.getElementById('style');
    currentCssStyle.href = style;

    updateThemeSwitcher()
}

updateThemes()