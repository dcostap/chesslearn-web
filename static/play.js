let socket = undefined
let reconnectTimer = undefined

function connect() {
    if (socket)
        socket.close()
    socket = io();
    socket.on('connect', function () {
        console.log("Just connected to socket.");
        if (reconnectTimer != undefined) {
            clearTimeout(reconnectTimer)
            reconnectTimer = undefined;
        }

        $("#loading-div").css("display", "none");

        let testing = false

        if (testing) {
            initDisplayedBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
            onMessageReceived(true, "test message")
            onMessageReceived(false, "test message")
        } else {
            createInitialButtons()
        }
    });

    socket.on("disconnect", (data) => {
        chessEngine = undefined
        board = undefined

        console.log("Just disconnected from socket.");

        $("#contents").empty()
        $("#contents").css("display", "none")
        $("#chat").empty()
        $("#chat").css("display", "none")

        $("#loading-div").css("display", "block");
        $("#below-loading-div").empty()
        $("#below-loading-div").append(`
        <div class="notification is-danger">
        <span class="icon">
        <i class="fas fa-exclamation-triangle"></i>
    </span>
            Lost connection with the server.
        </div>
        `)

        reconnectTimer = setTimeout(function () {
            console.log("Retrying connection to socket...");
            connect()
        }, 3400)
    });

    function processNewFen(fen) {
        if (chessEngine == undefined || board == undefined) {
            initDisplayedBoard(fen)
        } else {
            updateDisplayedBoardToFEN(fen)
        }
    }

    socket.on("game_end", (data) => {
        fen = data["fen"]
        let winner_color = data["winner_color"]

        processNewFen(fen)

        $("#top_of_board_after_name").empty()
        $("#bottom_of_board_after_name").empty()

        $("#bottom_of_chess_div").empty()

        let result = ""
        let color = "is-primary"
        if (winner_color != true && winner_color != false) {
            result = "It's a draw!"
        } else {
            let is_winner_white = winner_color == true
            if ((is_winner_white && am_I_white_player) || (!is_winner_white && !am_I_white_player)) {
                result = "You won!"
            } else {
                result = "You lost!"
                color = "is-danger"
            }
        }

        $("#bottom_of_chess_div").append(`
        <div class="block invalid-id-not">
            <div class="notification ${color}">
                <button class="delete" onclick="deleteAllNotificacions()"></button>
                <span class="icon">
                    <i class="fas fa-chess-king"></i>
                </span>
                ${result}
            </div>
        </div>
    `)
    })


    socket.on("new_turn", (data) => {
        fen = data["fen"]
        is_my_turn = data["is_your_turn"]

        console.log("Received a 'new turn' message: fen = " + fen);
        console.log("is_my_turn: " + is_my_turn);

        processNewFen(fen)

        if (data["last_move_was_invalid"] && is_my_turn) {
            $("#bottom_of_chess_div").empty()
            $("#bottom_of_chess_div").append(`
                <div class="block invalid-id-not">
                    <div class="notification is-danger">
                        <button class="delete" onclick="deleteAllNotificacions()"></button>
                        <span class="icon">
                            <i class="fas fa-exclamation-triangle"></i>
                        </span>
                        Error making the move.
                    </div>
                </div>
            `)
        }
    });
}

connect()

let chessEngine = undefined
let board = undefined
let player_id = undefined
let game_id = undefined
let fen = undefined
let is_my_turn = undefined
let am_I_white_player = undefined

function updateDisplayedBoardToFEN(fen) {
    chessEngine = new Chess(fen)
    board.position(chessEngine.fen())

    if (is_my_turn)
        am_I_white_player = chessEngine.turn() != "b"
    else
        am_I_white_player = chessEngine.turn() == "b"

    my_name = "<span class='has-text-info'>You</span>"
    anon = "Anonymous"

    $("#top_of_board_name").html(am_I_white_player ? anon : my_name)
    $("#bottom_of_board_name").html(!am_I_white_player ? anon : my_name)

    // repetitive code ahead, I'm too lazy to clean it
    $("#top_of_board_icon").removeClass("fa-user-secret")
    $("#top_of_board_icon").removeClass("fa-user-alt")
    $("#bottom_of_board_icon").removeClass("fa-user-secret")
    $("#bottom_of_board_icon").removeClass("fa-user-alt")

    $("#top_of_board_icon").addClass(am_I_white_player ? "fa-user-secret" : "fa-user-alt")
    $("#bottom_of_board_icon").addClass(!am_I_white_player ? "fa-user-secret" : "fa-user-alt")

    $("#top_of_board_after_name").empty()
    $("#bottom_of_board_after_name").empty()
    let element = chessEngine.turn() == "b" ? $("#top_of_board_after_name") : $("#bottom_of_board_after_name")

    let text = is_my_turn ? "Your turn" : "His turn"
    let color = is_my_turn ? "is-info" : "is-danger"

    element.html(`
        <span class="tag ${color} is-medium ml-3">${text}</span>
    `)
}

function onMadeMove(san) {
    clearAllNotifications()
    socket.emit('move', {
        "player_id": player_id,
        "game_id": game_id,
        "move_san": san,
    });
}

function clearAllNotifications() {
    $(".invalid-id-not").each(function (index, element) {
        element.remove()
    })
}

function onInvalidGameID() {
    clearAllNotifications()
    $("#contents").before(`
        <div class="block invalid-id-not">
        <div class="notification is-danger">
        <button class="delete" onclick="deleteAllNotificacions()"></button>
        <span class="icon">
        <i class="fas fa-exclamation-triangle"></i>
    </span>
            Invalid Game ID.
        </div>
        </div>
`)
}

function createInitialButtons() {
    $("#contents").css("display", "block");
    $("#chat").css("display", "none");
    $("#chat-messages").empty();
    $.get("./html_snippets/new_game_buttons.html", function (data) {
        $("#contents").append(data)

        let createBt = $("#create-game-button")
        let joinBt = $("#join-game-button")
        createBt.on("click", function () {
            createBt.addClass("is-loading")
            socket.emit('new_game', {});
        })

        joinBt.on("click", function (event) {
            var game_id = prompt("Enter game ID:");

            if (game_id != null && game_id != "" && game_id.length < 15) {
                game_id = game_id.trim()
                joinBt.addClass("is-loading")
                socket.emit('join_game', { "game_id": game_id });
            } else {
                if (game_id != null) {
                    onInvalidGameID()
                    joinBt.removeClass("is-loading")
                }
            }
        })

        socket.addEventListener("error_joining_game", (data) => {
            onInvalidGameID()
        })

        socket.addEventListener("joined_game", (data) => {
            player_id = data["player_id"]
            game_id = data["game_id"]

            console.log("Joined Game, id: " + game_id)

            if (!data["game_full"]) {
                createBt.removeClass("is-loading")
                createBt.attr("disabled", true)
                $("#beneath_buttons").empty()
                $("#beneath_buttons").append(`
                    <div class="box">
                        <p><sub><em>(Send this ID to a friend)</em></sub></p>
                        <span class="is-size-5">Game ID:</span>
                        <span class="is-size-3 ml-3">${game_id}</span>
                    </div>
                    <span class="tag is-info">
                        Game created, waiting for another player...
                        <div class="loader is-loading is-size-5 ml-2"/>
                    </span>
            `)
            }
        });

        socket.addEventListener("new_message", (data) => {
            let isMine = data["is_yours"]
            let text = data["text"]

            onMessageReceived(isMine, text)
        });
    })
}

function onMessageReceived(isMine, text) {
    console.log("Received chat message: " + text);

    $.get("html_snippets/heart_button.html", function (heartButton) {
        let icon = isMine ? "fa-user-alt" : "fa-user-secret";
        let name = isMine ? "You" : "Anonymous";

        $("#chat-messages").append(`
        <div class="block ml-2 mt-1">
            <article class="media">
                <div class="media-left">
                    <span class="icon">
                        <i class="fas fa-2x ${icon}"></i>
                    </span>
                </div>
                <div class="media-content">
                    <div class="content">
                        <p>
                        <strong>${name}</strong>
                        <br>
                        ${text}
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">

                        </div>
                    </nav>
                </div>
            </article>
        </div>
        `);
    });
}

function onHeartClick() {
    console.log("heart click");
}

$(document).keydown(function (event) {
    let chatInput = $("#chat-messages-input")
    if (chatInput.is(":focus") && event.key == "Enter") {
        console.log("Wrote chat message: " + chatInput.val());
        socket.emit("send_message", {
            "player_id": player_id,
            "game_id": game_id,
            "text": chatInput.val()
        });
        chatInput.val("");
    }
});

function initDisplayedBoard(fen) {
    $.get("html_snippets/chessboard_div.html", function (data) {
        $("#contents").empty();
        $("#chat-messages").empty();
        $("#contents").css("display", "block");
        $("#chat").css("display", "block");
        $("#contents").append(data)

        var config = {
            draggable: true,
            position: 'start',
            onDragStart: onDragStart,
            onDrop: onDrop,
            onSnapEnd: onSnapEnd
        }

        board = Chessboard('board', config);

        function onDragStart(source, piece, position, orientation) {
            // do not pick up pieces if the game is over
            if (chessEngine.game_over()) return false

            // only pick up pieces for the side to move
            if (am_I_white_player == undefined) return true;

            console.log("drag piece: " + piece[0]);
            console.log("current turn: " + chessEngine.turn());

            return (
                (chessEngine.turn() === 'w' && am_I_white_player) || (chessEngine.turn() === 'b' && !am_I_white_player)
                &&
                (piece[0] === chessEngine.turn()) // can only drag my pieces
            )
        }

        function onDrop(source, target) {
            // see if the move is legal
            var move = chessEngine.move({
                from: source,
                to: target,
                promotion: 'q' // NOTE: always promote to a queen for example simplicity
            })

            // illegal move
            if (move === null) {
                console.log("Made an invalid move");
                return 'snapback'
            }

            console.log("Made a valid move: " + move);
            onMadeMove(move.san)
        }

        // update the board position after the piece snap
        // for castling, en passant, pawn promotion
        function onSnapEnd() {
            board.position(chessEngine.fen())
        }

        updateBoardSizing()
        updateDisplayedBoardToFEN(fen)
    })
}

function updateBoardSizing() {
    // don't let the board get out of the view
    $("#chess-div").css({
        "width": "60vmin",
    })
    if (board) {
        board.resize()
    }
}

window.onresize = function () {
    updateBoardSizing()
}