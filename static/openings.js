let currentOpening = undefined

let chessEngine = undefined
let board = undefined

document.addEventListener("DOMContentLoaded", function (event) {
    // starting position
    processNewFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
})

async function fetchNewOpeningInfo(fen) {
    $("#info-column").empty()
    $("#info-column").append(
        `
        <div class="animate__animated animate__bounceIn animate__delay-1s">
        <div class="block is-size-4 has-text-centered">Loading opening info...</div>
        <div class="block">
            <progress class="progress is-small is-primary" max="100"></progress>
        </div>
        </div>
        `
    )

    console.log("fen: " + fen);
    let fen64 = btoa(fen)
    console.log("fen base64: " + fen64);

    fetch('get_opening/' + fen64)
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(blob => {
            currentOpening = blob
            setupCurrentOpeningInfo()
        })
        .catch(e => {
            console.log('There has been a problem with your fetch operation: ' + e.message);

            $("#info-column").empty()
            $("#info-column").append(
                `
                <div class="animate__animated animate__bounceIn">
                <div class="block is-size-4 has-text-centered">There was a problem trying to establish connection with the server...</div>
                </div>
                `
            )
        });
}

function setupCurrentOpeningInfo() {
    $("#info-column").empty()

    console.log("current opening: " + JSON.stringify(currentOpening));

    let openingNameHtml = "";

    if (currentOpening["opening_name"]) {
        openingNameHtml = `
            <div class="block has-text-centered m-4">
                <span class="icon">
                    <i class="fas fa-2x fa-book-open"></i>
                </span>
                <span class="has-text-centered ml-4">
                    <span class="is-size-5">${currentOpening["opening_name"]}</span>
                </span>
            </div>
        `
    }

    let movesHtml = "";

    let continuations = currentOpening["common_continuations"]

    let totalTimesPlayed = 0
    for (op in continuations) {
        let opening = continuations[op]
        totalTimesPlayed += opening["times_played"]
    }

    function getPopularityFromMove(times_played) {
        return (times_played / totalTimesPlayed) * 100
    }

    // sort by popularity
    if (continuations)
        continuations = continuations.sort(function (a, b) {
            let pop1 = getPopularityFromMove(a["times_played"])
            let pop2 = getPopularityFromMove(b["times_played"])
            return pop1 < pop2 ? 1 : pop1 > pop2 ? -1 : 0
        })

    for (op in continuations) {
        let opening = continuations[op]
        let popularity = getPopularityFromMove(opening["times_played"])

        let starHtml = `
        <span class="icon">
            <i class="fas fa-star"></i>
        </span>
        `

        let ratingInt = opening["move_rating"] * 2.7
        ratingInt = Math.floor(ratingInt)
        if (chessEngine.turn() == "b") ratingInt *= -1
        if (ratingInt < 0) ratingInt = 0
        if (ratingInt > 3) ratingInt = 3

        let rating = ""
        for (var i = 0; i < ratingInt; i++) {
            rating += starHtml
        }

        movesHtml += `
            <tr>
                <td>
                    <a onclick="chessEngine.move('${opening["move"]}'); onMadeMove(); processNewFen(chessEngine.fen())">
                    ${opening["move"]}
                    </a>
                </td>
                <td><progress class="progress is-primary" value="${popularity}" max="100"></progress></td>
                <td>${rating}</td>
            </tr>
        `
    }

    let tableHtml = `
    <table class="table is-fullwidth">
    <thead>
        <tr>
            <th><abbr title="The move in SAN notation">Move</abbr></th>
            <th><abbr>Popularity</abbr></th>
            <th><abbr title="Rating aggregated from data in real chess games where this move was played">Rating</abbr></th>
        </tr>
    </thead>
    <tbody>
        ${movesHtml}
    <tbody>
    </table>
    `

    $("#info-column").append(`
        <div class="animate__animated animate__fadeIn animate__faster block has-text-centered">
            ${openingNameHtml}
            <div class="block has-text-centered" id="move_info_div" style="height: 60vh; overflow: auto;">
${tableHtml}
            </div>
            <button class="button" onclick="onClickGoBackButton()">
                <span class="icon">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span>Go back</span>
            </button>
        </div>
        </div>
    `)
}

function onClickGoBackButton() {
    console.log("UNDONE");
    if (chessEngine.undo()) {
        processNewFen(chessEngine.fen())
    }
}

function processNewFen(fen) {
    if (chessEngine == undefined || board == undefined) {
        initDisplayedBoard(fen)
    } else {
        updateDisplayedBoardToFEN(fen)
    }

    fetchNewOpeningInfo(fen)
}

function updateDisplayedBoardToFEN(fen) {
    if (chessEngine == undefined || chessEngine.fen() != fen) {
        chessEngine = new Chess(fen)
    }
    console.log("Validity of the fen position: " + chessEngine.validate_fen(fen))

    updateDisplayedBoardToEngine()
    updateNameTags()
}

function updateNameTags() {
    $("#top_of_board_name").html("")
    $("#bottom_of_board_name").html("")

    // repetitive code ahead, I'm too lazy to clean it
    $("#top_of_board_icon").removeClass("fa-user-alt")
    $("#bottom_of_board_icon").removeClass("fa-user-alt")

    $("#top_of_board_icon").addClass("fa-user-alt")
    $("#bottom_of_board_icon").addClass("fa-user-alt")

    $("#top_of_board_after_name").empty()
    $("#bottom_of_board_after_name").empty()
    let element = chessEngine.turn() == "b" ? $("#top_of_board_after_name") : $("#bottom_of_board_after_name")
    console.log("CURRENT TURN" + chessEngine.turn() == "b");
    let text = "Your turn"
    let color = "is-info"

    element.html(`
    <span class="tag ${color} is-medium ml-3">${text}</span>
    `)
}

function updateDisplayedBoardToEngine() {
    board.position(chessEngine.fen())
}

function onMadeMove(san) {
    fetchNewOpeningInfo(chessEngine.fen())
    updateNameTags()
    clearAllNotifications()
}

function clearAllNotifications() {
    $(".invalid-id-not").each(function (index, element) {
        element.remove()
    })
}

function initDisplayedBoard(fen) {
    $.get("html_snippets/chessboard_div.html", function (data) {
        $("#contents").empty();
        $("#contents").css("display", "block");
        $("#contents").append(data)

        var config = {
            draggable: true,
            position: 'start',
            onDragStart: onDragStart,
            onDrop: onDrop,
            onSnapEnd: onSnapEnd
        }

        function onDragStart(source, piece, position, orientation) {
            // do not pick up pieces if the game is over
            if (chessEngine.game_over()) return false

            return (
                (piece[0] === chessEngine.turn())
            )
        }

        function onDrop(source, target) {
            // see if the move is legal
            var move = chessEngine.move({
                from: source,
                to: target,
                promotion: 'q' // NOTE: always promote to a queen for example simplicity
            })

            // illegal move
            if (move === null) {
                console.log("Made an invalid move");
                return 'snapback'
            }

            console.log("Made a valid move: " + move);
            onMadeMove(move.san)
        }

        // update the board position after the piece snap
        // for castling, en passant, pawn promotion
        function onSnapEnd() {
            board.position(chessEngine.fen())
        }

        board = Chessboard('board', config);
        updateBoardSizing()
        updateDisplayedBoardToFEN(fen)
    })
}

function updateBoardSizing() {
    // don't let the board get out of the view
    $("#chess-div").css({
        "width": "60vmin",
    })
    if (board) {
        board.resize()
    }
}

window.onresize = function () {
    updateBoardSizing()
}