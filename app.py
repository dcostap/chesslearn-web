"""Flask App Project."""

import chess_game_backend
import json
import string
import random
import sys
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from flask import Flask, jsonify, redirect, request
from flask_socketio import SocketIO
from flask_socketio import send, emit
from flask_restful import Resource, Api
import sqlite3
import base64

app = Flask(__name__, static_url_path='', static_folder='static',
            template_folder='web/templates')

rest_api = Api(app)

socketio = SocketIO(app, logger=True, engineio_logger=True,
                    cors_allowed_origins="*")

chess_game_backend.init(socketio)


@app.route('/')
def index():
    return redirect("/index.html")

puzzles = []

with open("puzzles.json") as file:
    puzzles = json.load(file)

class PuzzleAPIRest(Resource):
    def get(self):
        return random.choice(puzzles)

rest_api.add_resource(PuzzleAPIRest, '/new_puzzle')

def connect_to_db():
    conn = None
    try:
        conn = sqlite3.connect("openings.db")
    except Exception as e:
        print(e)

    return conn

# testing openings API: http://127.0.0.1:5000/get_opening/cm5icWtibnIvcHBwcHBwcHAvOC84LzRQMy84L1BQUFAxUFBQL1JOQlFLQk5SIGIgS1FrcSBlMyAwIDE=


class OpeningAPIRest(Resource):
    def get(self, fen_base64):
        fen = base64.b64decode(fen_base64).decode('utf-8')
        db_connection = connect_to_db()
        cursor = db_connection.cursor()
        cursor.execute("SELECT ID_posicion, nombre_apertura FROM posicion_tablero WHERE FEN = ?", (fen,))
        row = cursor.fetchone()

        if row:
            cursor.execute("SELECT ID_continuacion," +
                "       movimiento," +
                "       veces_jugado," +
                "       victorias_blanco," +
                "       empates," +
                "       victorias_negro," +
                "       valoracion_movimiento" +
                "  FROM continuacion_comun WHERE ID_posicion = ?", (row[0], ))

            common_continuations = []
            for cont in cursor.fetchall():
                common_continuations.append({
                    "ID_cont": cont[0],
                    "move": cont[1],
                    "times_played": cont[2],
                    "white_wins": cont[3],
                    "draws": cont[4],
                    "black_wins": cont[5],
                    "move_rating": cont[6],
                })

            return {
                "opening_name": row[1],
                "common_continuations": common_continuations
            }
        else:
            return {

            }


rest_api.add_resource(OpeningAPIRest, '/get_opening/<string:fen_base64>')

if __name__ == '__main__':
    if len(sys.argv) > 1:  # production env on heroku
        socketio.run(app, debug=True, host="0.0.0.0", port=int(sys.argv[1]))
    else:
        socketio.run(app, debug=True)

# puzzles database
# http://wtharvey.com/m8n2.txt


