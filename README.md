# Chesslearn Web

https://dcostap-chess.herokuapp.com/index.html

Este repositorio contiene el front y back-end de la web.

Se utiliza una cuenta gratuita de **Heroku** para disponer de servidor web.

## Estructura

Este mismo repositorio es el que se despliega a **Heroku**. La carpeta raíz contiene algunos [archivos](Procfile) que configuran el despliegue, que *Heroku* ejecuta en _containers_ web.

**Gunicorn** es el *Web Server Gateway Interface* (WSGI) utilizado para el despliegue.

- Back-end
  - Se usa `python` y `Flask`. [app.py](./app.py) es el punto de inicio.
- Front-end
  - Dentro de `./static/`
  - Contiene principalmente HTML acompañado de Javascript, y [CSS autogenerado](#css-sass-y-bulma).
    - `html_snippets/`: Esta carpeta contiene HTML reusado en varias páginas.
    - `sass/`: Carpeta que contiene los archivos SASS que definen los estilos de la página. [Se compilan a CSS con un comando.](#css-sass-y-bulma) (los CSS resultantes se guardan en la carpeta `css/`)
    - `index.html` & `index.js`: Pantalla principal
    - `play.html` & `play.js`: Pantalla donde puedes jugar partidas con otro jugador.
    - `puzzles.html` & `puzzles.js`: Pantalla de Puzzles.
    - `openings.html` & `openings.js`: Pantalla de *Análisis de Aperturas*.
    - `theme_switcher.js`: Javascript cargado en todas las pantallas; permite cambiar el _look_ de la web entre _light / dark_
    - `load_navbar.js`: Javascript cargado en todas las pantallas; carga el HTMl compartido de la barra de navegación.
- Static File Server
  - Para simplificar se utiliza (¿temporalmente?) el mismo _back-end framework_ `Flask` para servir las páginas y recursos estáticos. En un futuro se migraría a `nginx`.

## Despliegue de la web

### Pruebas

Instala python y el gestor de paquetes `pip`.

Instala las dependencias de paquetes python del proyecto, que se encuentran en el [Pipfile](Pipfile), bien manualmente (`pip install dependencia`), o usando pipenv (estando situado en la carpeta en la que se encuentra Pipfile):

`pip install pipenv`

`pipenv install`


Para lanzar el servidor Flask de manera local:

`python main.py`

### CSS: Sass y Bulma

Para estilizar la página se usó [Bulma](https://bulma.io/).

Se ha seguido la [guía de personalización de Bulma](https://bulma.io/documentation/customize/with-node-sass/) para tener una copia local de la hoja de estilos, en la carpeta `./static/sass/`. Esto nos permite añadir modificaciones a los estilos.

**Para compilar Sass a CSS, ejecuta ```npm run css-build```.**

> Los scripts de NPM se encuentran definidos en `./static/package.json`

### Despliegue online

Esta parte depende de qué servicios vayas a usar para realizar el _hosting_. Este repositorio está preparado con las configuraciones para el despliegue en Heroku, el cual me permite realizar el despliegue mediante un simple `git push heroku` (habiendo añadido previamente el repositorio que nos proporciona Heroku a la lista de `git remote`).

## Licencia

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0)
